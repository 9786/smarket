/*
Navicat MySQL Data Transfer

Source Server         : locahost
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : yalei_os

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2015-01-26 15:49:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ts_classes`
-- ----------------------------
DROP TABLE IF EXISTS `ts_classes`;
CREATE TABLE `ts_classes` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `parentid` mediumint(9) NOT NULL DEFAULT '0',
  `path` varchar(20) DEFAULT NULL,
  `ordersn` mediumint(9) NOT NULL DEFAULT '0',
  `sequence` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_classes
-- ----------------------------
INSERT INTO `ts_classes` VALUES ('24', '西药药品', '0', ',0,24,', '1', '1');
INSERT INTO `ts_classes` VALUES ('25', '中药成药', '0', ',0,25,', '2', '5');
INSERT INTO `ts_classes` VALUES ('26', '保健食品', '0', ',0,26,', '3', '6');
INSERT INTO `ts_classes` VALUES ('27', '美容护肤', '0', ',0,27,', '4', '7');
INSERT INTO `ts_classes` VALUES ('28', '医疗器械', '0', ',0,28,', '5', '8');
INSERT INTO `ts_classes` VALUES ('29', '成人用品', '0', ',0,29,', '6', '9');
INSERT INTO `ts_classes` VALUES ('30', '参茸饮片', '0', ',0,30,', '7', '10');
INSERT INTO `ts_classes` VALUES ('31', '生活日化', '0', ',0,31,', '8', '11');
INSERT INTO `ts_classes` VALUES ('32', '减肥瘦身', '24', ',0,24,32,', '3', '4');
INSERT INTO `ts_classes` VALUES ('33', '抗生素药', '24', ',0,24,33,', '2', '3');
INSERT INTO `ts_classes` VALUES ('34', '解热镇痛', '24', ',0,24,34,', '1', '2');
INSERT INTO `ts_classes` VALUES ('35', '杂货', '0', ',0,35,', '10', '0');

-- ----------------------------
-- Table structure for `ts_customers`
-- ----------------------------
DROP TABLE IF EXISTS `ts_customers`;
CREATE TABLE `ts_customers` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '0',
  `linkman` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `memo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_customers
-- ----------------------------
INSERT INTO `ts_customers` VALUES ('1', '贺周明', '1', '', '13918903660', 'hecity@163.com', '四平路311号乙座1603', '');
INSERT INTO `ts_customers` VALUES ('2', '程显鹏', '1', '', '', 'paul_cheng@yicang.com', '浙江杭州', '');
INSERT INTO `ts_customers` VALUES ('3', '刘进', '1', '', '', '', '', '');
INSERT INTO `ts_customers` VALUES ('4', '贺小明', '1', '', '', '', '', '');
INSERT INTO `ts_customers` VALUES ('5', '郑照琴', '1', '', '', '', '', '');
INSERT INTO `ts_customers` VALUES ('6', '杭州亚细亚技术有限公司', '2', '', '', '', '', '');

-- ----------------------------
-- Table structure for `ts_managers`
-- ----------------------------
DROP TABLE IF EXISTS `ts_managers`;
CREATE TABLE `ts_managers` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `realname` varchar(20) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '1',
  `phone` varchar(30) DEFAULT NULL,
  `rights` text,
  `regdate` int(11) NOT NULL DEFAULT '0',
  `logdate` int(11) NOT NULL DEFAULT '0',
  `ipaddress` varchar(15) DEFAULT NULL,
  `logcount` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_managers
-- ----------------------------
INSERT INTO `ts_managers` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '0', null, 'admin', '1', null, 'administrator', '1410315346', '1422258127', '192.168.0.89', '6');
INSERT INTO `ts_managers` VALUES ('2', 'guest', 'e10adc3949ba59abbe56e057f20f883e', '0', '', 'guest', '1', '13306350066', 'purchases_view;suppliers_view;orders_view;customers_view;stocks_view;income_view;payout_view;moving_view;shops_view;products_view;classes_view;system_view;managers_view', '1414456906', '1422239926', '192.168.0.89', '2');

-- ----------------------------
-- Table structure for `ts_moving`
-- ----------------------------
DROP TABLE IF EXISTS `ts_moving`;
CREATE TABLE `ts_moving` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `shopid_from` mediumint(9) NOT NULL DEFAULT '0',
  `shop_from` varchar(40) DEFAULT NULL,
  `shopid_to` mediumint(9) NOT NULL DEFAULT '0',
  `shop_to` varchar(40) DEFAULT NULL,
  `productid` mediumint(9) NOT NULL DEFAULT '0',
  `product` varchar(60) DEFAULT NULL,
  `spec` varchar(20) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `number` float(10,2) NOT NULL DEFAULT '0.00',
  `memo` text,
  `thedate` int(11) NOT NULL DEFAULT '0',
  `timestocked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_moving
-- ----------------------------
INSERT INTO `ts_moving` VALUES ('4', '5', '外高桥店', '6', '饮片厂', '22', '秋水仙碱片', '0.5mg×20s', '盒', '6.00', 'test', '1244217600', '0');
INSERT INTO `ts_moving` VALUES ('6', '6', '饮片厂', '5', '外高桥店', '28', '雅漾清爽洁肤凝胶', '200ml', '瓶', '11.00', '', '1241712000', '1247042199');
INSERT INTO `ts_moving` VALUES ('7', '3', '乳山店', '5', '外高桥店', '30', '参归养血片', '盒', '个', '1.00', '', '1414425600', '0');

-- ----------------------------
-- Table structure for `ts_orders`
-- ----------------------------
DROP TABLE IF EXISTS `ts_orders`;
CREATE TABLE `ts_orders` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `shopid` mediumint(9) NOT NULL DEFAULT '0',
  `shop` varchar(40) DEFAULT NULL,
  `productid` mediumint(9) NOT NULL DEFAULT '0',
  `product` varchar(60) DEFAULT NULL,
  `spec` varchar(20) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `pricesell` float(10,2) NOT NULL DEFAULT '0.00',
  `number` float(10,2) NOT NULL DEFAULT '0.00',
  `customerid` mediumint(9) NOT NULL DEFAULT '0',
  `customer` varchar(60) DEFAULT NULL,
  `memo` text,
  `thedate` int(11) NOT NULL DEFAULT '0',
  `timestocked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_orders
-- ----------------------------
INSERT INTO `ts_orders` VALUES ('7', '3', '乳山店', '24', '安乃近片(诺瓦经)', '500mg×24s', '盒', '2.20', '10.00', '3', '刘进', '', '1245600000', '1247040300');
INSERT INTO `ts_orders` VALUES ('8', '6', '饮片厂', '25', '薇姿油脂调护修润日霜', '50ml', '瓶', '178.60', '2.00', '2', '程显鹏', '', '1245600000', '1247040297');
INSERT INTO `ts_orders` VALUES ('9', '3', '乳山店', '26', '薇姿泉之净纯净洁面啫喱', '125ml', '瓶', '150.10', '22.00', '1', '贺周明', '', '1410278400', '0');

-- ----------------------------
-- Table structure for `ts_products`
-- ----------------------------
DROP TABLE IF EXISTS `ts_products`;
CREATE TABLE `ts_products` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `spec` varchar(20) DEFAULT NULL,
  `classid` mediumint(9) NOT NULL DEFAULT '0',
  `path` varchar(20) DEFAULT NULL,
  `pricebuy` float(10,2) DEFAULT NULL,
  `pricesell` float(10,2) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `intro` text,
  `stocks` float(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_products
-- ----------------------------
INSERT INTO `ts_products` VALUES ('22', '秋水仙碱片', '0.5mg×20s', '34', ',0,24,34,', '24.20', '26.80', '盒', '', '0.00');
INSERT INTO `ts_products` VALUES ('23', '肿抗片(肿节风片)', '360s×0.25g', '34', ',0,24,34,', '125.50', '131.10', '盒', '', '100.00');
INSERT INTO `ts_products` VALUES ('24', '安乃近片(诺瓦经)', '500mg×24s', '34', ',0,24,34,', '1.60', '2.20', '盒', '', '430.00');
INSERT INTO `ts_products` VALUES ('25', '薇姿油脂调护修润日霜', '50ml', '27', ',0,27,', '158.20', '178.60', '瓶', '', '98.00');
INSERT INTO `ts_products` VALUES ('26', '薇姿泉之净纯净洁面啫喱', '125ml', '27', ',0,27,', '128.80', '150.10', '瓶', '', '23.00');
INSERT INTO `ts_products` VALUES ('27', '薇姿温泉矿物保湿霜(清爽型)', '50ML', '27', ',0,27,', '124.20', '178.60', '瓶', '', '0.00');
INSERT INTO `ts_products` VALUES ('28', '雅漾清爽洁肤凝胶', '200ml', '27', ',0,27,', '112.80', '159.60', '瓶', '', '50.00');
INSERT INTO `ts_products` VALUES ('29', '理肤泉立润保湿隔离乳液', '50ml', '27', ',0,27,', '180.00', '226.10', '瓶', '', '0.00');
INSERT INTO `ts_products` VALUES ('30', '参归养血片', '盒', '25', ',0,25,', '33.00', '66.00', '个', '参归养血片', '10.00');

-- ----------------------------
-- Table structure for `ts_purchases`
-- ----------------------------
DROP TABLE IF EXISTS `ts_purchases`;
CREATE TABLE `ts_purchases` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `shopid` mediumint(9) NOT NULL DEFAULT '0',
  `shop` varchar(40) DEFAULT NULL,
  `productid` mediumint(9) NOT NULL DEFAULT '0',
  `product` varchar(60) DEFAULT NULL,
  `spec` varchar(20) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `pricebuy` float(10,2) NOT NULL DEFAULT '0.00',
  `number` float(10,2) NOT NULL DEFAULT '0.00',
  `supplierid` mediumint(9) NOT NULL DEFAULT '0',
  `supplier` varchar(60) DEFAULT NULL,
  `memo` text,
  `thedate` int(11) NOT NULL DEFAULT '0',
  `timestocked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_purchases
-- ----------------------------
INSERT INTO `ts_purchases` VALUES ('8', '6', '饮片厂', '25', '薇姿油脂调护修润日霜', '50ml', '瓶', '158.20', '100.00', '5', '上海薇姿化妆品有限公司', '', '1245254400', '1247040268');
INSERT INTO `ts_purchases` VALUES ('9', '6', '饮片厂', '28', '雅漾清爽洁肤凝胶', '200ml', '瓶', '112.80', '50.00', '4', '上海银路保键品有限公司', '', '1245254400', '1247040265');
INSERT INTO `ts_purchases` VALUES ('10', '6', '饮片厂', '24', '安乃近片(诺瓦经)', '500mg×24s', '盒', '1.60', '400.00', '5', '上海薇姿化妆品有限公司', '', '1245254400', '1247040262');
INSERT INTO `ts_purchases` VALUES ('11', '3', '乳山店', '24', '安乃近片(诺瓦经)', '500mg×24s', '盒', '1.60', '40.00', '6', '上海医药(集团)有限公司', '', '1245254400', '1247040258');
INSERT INTO `ts_purchases` VALUES ('12', '6', '饮片厂', '23', '肿抗片(肿节风片)', '360s×0.25g', '盒', '125.50', '100.00', '6', '上海医药(集团)有限公司', '', '1245254400', '1247040256');
INSERT INTO `ts_purchases` VALUES ('13', '6', '饮片厂', '23', '肿抗片(肿节风片)', '360s×0.25g', '盒', '125.50', '40.00', '6', '上海医药(集团)有限公司', '', '1245254400', '0');
INSERT INTO `ts_purchases` VALUES ('14', '5', '外高桥店', '26', '薇姿泉之净纯净洁面啫喱', '125ml', '瓶', '128.80', '23.00', '6', '上海医药(集团)有限公司', '', '1245600000', '1247040250');
INSERT INTO `ts_purchases` VALUES ('15', '4', '严杨店', '22', '秋水仙碱片', '0.5mg×20s', '盒', '24.20', '80.00', '4', '上海鑫路保键品有限公司', '秋水仙碱片', '1410278400', '0');
INSERT INTO `ts_purchases` VALUES ('16', '4', '严杨店', '30', '参归养血片', '盒', '个', '33.00', '10.00', '6', '上海医药(集团)有限公司', '参归养血片', '1414425600', '1414458313');

-- ----------------------------
-- Table structure for `ts_shops`
-- ----------------------------
DROP TABLE IF EXISTS `ts_shops`;
CREATE TABLE `ts_shops` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_shops
-- ----------------------------
INSERT INTO `ts_shops` VALUES ('3', '乳山店');
INSERT INTO `ts_shops` VALUES ('4', '严杨店');
INSERT INTO `ts_shops` VALUES ('5', '外高桥店');
INSERT INTO `ts_shops` VALUES ('6', '饮片厂');
INSERT INTO `ts_shops` VALUES ('7', '德庆堂店');
INSERT INTO `ts_shops` VALUES ('8', '永安店');

-- ----------------------------
-- Table structure for `ts_stocks`
-- ----------------------------
DROP TABLE IF EXISTS `ts_stocks`;
CREATE TABLE `ts_stocks` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `shopid` mediumint(9) NOT NULL DEFAULT '0',
  `shop` varchar(40) DEFAULT NULL,
  `productid` mediumint(9) NOT NULL DEFAULT '0',
  `product` varchar(60) DEFAULT NULL,
  `spec` varchar(20) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `stocks` float(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_stocks
-- ----------------------------
INSERT INTO `ts_stocks` VALUES ('9', '5', '外高桥店', '26', '薇姿泉之净纯净洁面啫喱', '125ml', '瓶', '23.00');
INSERT INTO `ts_stocks` VALUES ('10', '6', '饮片厂', '23', '肿抗片(肿节风片)', '360s×0.25g', '盒', '100.00');
INSERT INTO `ts_stocks` VALUES ('11', '3', '乳山店', '24', '安乃近片(诺瓦经)', '500mg×24s', '盒', '30.00');
INSERT INTO `ts_stocks` VALUES ('12', '6', '饮片厂', '24', '安乃近片(诺瓦经)', '500mg×24s', '盒', '400.00');
INSERT INTO `ts_stocks` VALUES ('13', '6', '饮片厂', '28', '雅漾清爽洁肤凝胶', '200ml', '瓶', '39.00');
INSERT INTO `ts_stocks` VALUES ('14', '6', '饮片厂', '25', '薇姿油脂调护修润日霜', '50ml', '瓶', '98.00');
INSERT INTO `ts_stocks` VALUES ('22', '5', '外高桥店', '28', '雅漾清爽洁肤凝胶', '200ml', '瓶', '11.00');
INSERT INTO `ts_stocks` VALUES ('23', '4', '严杨店', '30', '参归养血片', '盒', '个', '10.00');

-- ----------------------------
-- Table structure for `ts_suppliers`
-- ----------------------------
DROP TABLE IF EXISTS `ts_suppliers`;
CREATE TABLE `ts_suppliers` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) DEFAULT NULL,
  `linkman` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `memo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ts_suppliers
-- ----------------------------
INSERT INTO `ts_suppliers` VALUES ('4', '上海鑫路保键品有限公司', '', '', '', '');
INSERT INTO `ts_suppliers` VALUES ('5', '上海薇姿化妆品有限公司', '', '', '', '');
INSERT INTO `ts_suppliers` VALUES ('6', '上海医药(集团)有限公司', '', '', '', '');
